﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveController : MonoBehaviour
{
    private Rigidbody2D rig;

    public float moveAccel;
    public float maxSpeed;

    public float jumpAccel;
    public float doubleJumpAccel;
    public float gravityOnGroundPound;
    public float initialGravity;
    private bool isJumping;
    private bool isDoubleJump;
    private bool hasDoubleJumped;
    private bool isOnGround;

    private bool isGroundPound;

    public float groundRaycastDistance;
    public LayerMask groundLayerMask;

    private Animator anim;
    private CharacterSoundController characterSound;

    public ScoreController score;
    public float scoreRatio;
    private float lastPositionX;

    public GameObject gameOverScreen;
    public float fallPositionY;

    public CameraMoveController gameCamera;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        characterSound = GetComponent<CharacterSoundController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (isOnGround)
            {
                isJumping = true;

                characterSound.PlayJump();
            }

            if (!isOnGround && !hasDoubleJumped)
            {
                isDoubleJump = true;
                hasDoubleJumped = true;
                characterSound.PlayJump();
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (!isOnGround)
            {
                isGroundPound = true;
            }
        }

        anim.SetBool("isOnGround", isOnGround);
        anim.SetBool("isDoubleJump", isDoubleJump);
        anim.SetBool("isGroundPound", isGroundPound);

        int distancePassed = Mathf.FloorToInt(transform.position.x - lastPositionX);
        int scoreIncrement = Mathf.FloorToInt(distancePassed / scoreRatio);

        if(scoreIncrement > 0)
        {
            score.IncreaseCurrentScore(scoreIncrement);
            lastPositionX += distancePassed;
        }

        if(transform.position.y < fallPositionY)
        {
            GameOver();
        }
    }

    private void FixedUpdate()
    {
        Vector2 velocityVector = rig.velocity;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundRaycastDistance, groundLayerMask);
        if (hit)
        {
            if (!isOnGround && rig.velocity.y <= 0)
            {
                isOnGround = true;
                hasDoubleJumped = false;
                isGroundPound = false;
                rig.gravityScale = initialGravity;
            }
        }
        else
        {
            isOnGround = false;
        }

        velocityVector.x = Mathf.Clamp(velocityVector.x + moveAccel * Time.deltaTime, 0f, maxSpeed);
        if (isJumping)
        {
            velocityVector.y += jumpAccel;
            isJumping = false;
        }
        

        if(isGroundPound)
        {
            rig.gravityScale = gravityOnGroundPound;
        }
        else if (isDoubleJump)
        {
            velocityVector.y += doubleJumpAccel;
            isDoubleJump = false;
        }

        rig.velocity = velocityVector;
    }

    private void GameOver()
    {
        score.FinishScoring();
        gameCamera.enabled = false;
        gameOverScreen.SetActive(true);

        this.enabled = false;
    }

    private void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * groundRaycastDistance), Color.white);
    }
}
